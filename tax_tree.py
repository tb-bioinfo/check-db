import os

class KNode:
    def __init__(self, tax_id, parent):
        self.tax_id = tax_id
        self.parent = parent
    
    def display(self):
        print(f"Tax ID: {self.tax_id}, parent: {self.parent}")

class KTree:
    def __init__(self):
        self.root = None
        self.nodes = {}
    
    def __init__(self, database):
        self.root = None
        self.nodes = {}

        with open(f"{database}/taxonomy/nodes.dmp", "r") as nodes_file:
            orphans = {}
            for line in nodes_file:
                sline = line.split("\t|\t")
                tax_id = int(sline[0])
                parent = int(sline[1])
                parent_node = self.nodes.get(parent, None)
                new_node = KNode(tax_id, parent_node)
                if parent_node is None:
                    if tax_id==parent:
                        self.root = new_node
                        print(f"Root: {tax_id}")
                    else:
                        if parent not in orphans.keys():
                            orphans[parent] = set()
                        orphans[parent].add(new_node)
                child_nodes = orphans.get(tax_id, None)
                if child_nodes:
                    for child in child_nodes:
                        child.parent = new_node
                    del orphans[tax_id]
                self.nodes[tax_id] = new_node
            print(f"Orphan nodes: {orphans}")
            print("No parents:")
            [node.display() for node in self.nodes.values() if node.parent is None]

    def get_node(self, tax_id):
        return self.nodes.get(tax_id, None)

    def get_parent_of(self, tax_id):
        node = self.get_node(tax_id)
        if node is None:
            return -1
        return node.parent.tax_id
    
    def is_parent(self, parent, child):
        if parent==child:
            return True
        child = self.get_node(child)
        if child is None:
            return False
        if parent==self.root.tax_id:
            return True
        while child!=self.root:
            child = child.parent
            if parent==child.tax_id:
                return True
        return False
    
    def get_lca(self, tax_id_1, tax_id_2):
        node1 = self.get_node(tax_id_1)
        node2 = self.get_node(tax_id_2)

        ancestors1 = set()
        ancestors2 = set()

        current_node = node1
        while current_node is not None:
            ancestors1.add(current_node.tax_id)
            current_node = current_node.parent

        current_node = node2
        while current_node is not None:
            ancestors2.add(current_node.tax_id)
            if current_node.tax_id in ancestors1:
                return current_node.tax_id
            current_node = current_node.parent

        print(f"No LCA found for {tax_id_1} ({type(tax_id_1)}) and {tax_id_2} ({type(tax_id_2)})")
        print(ancestors1)
        print(ancestors2)
        return 0
