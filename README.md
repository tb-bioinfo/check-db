# Check-db

A tool to check the quality of [*Kraken 2*](https://github.com/DerrickWood/kraken2) databases.

It compares the predictions made by Kraken2 with the sequences in the database to determine if they are predicted correctly.

If a local Blast database is provided, it will compare the identity percentage of each sequence with the closest sequence. This will help you to determine wether the assignation errors are due to sequences of different species being too similar to discriminate.

The program should be run with the same Fasta files as the ones used to build the Kraken2 and Blast databases. The sequences should have the accession ID in the header.

## Usage

```
Usage: check-db.py [options]

Options:
  -h, --help            show this help message and exit
  -k KRAKEN_DB, --kraken-db=KRAKEN_DB
                        The Kraken database to check.
  -b BLAST_DB, --blast-db=BLAST_DB
                        (Optional) A local Blast database used to find the
                        closest neighbor of the sequences. It must contain the
                        sequences with the accession ID information (Option
                        -parse_seqids when building the database). If not
                        provided, the comparison will not be performed.
  -f FASTA_FILES, --fasta-files=FASTA_FILES
                        The Fasta file or directory containing the sequences
                        to check. Headers must contain the same accession ID
                        as in the database.
  -n N_TESTS, --n-tests=N_TESTS
                        The number of sequences to use for the test. If not
                        provided, all the sequences will be used.
  -c CONFIDENCE, --confidence=CONFIDENCE
                        (Optional) Confidence Kraken parmeter
  -g MINIMUM_HIT_GROUPS, --minimum-hit-groups=MINIMUM_HIT_GROUPS
                        (Optional) Minimum hit groups Kraken parmeter
  -q MINIMUM_BASE_QUALITY, --minimum-base-quality=MINIMUM_BASE_QUALITY
                        (Optional) Minimum base quality Kraken parmeter
```