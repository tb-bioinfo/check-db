from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import random
import os
import sys
import subprocess
from tqdm import tqdm
import optparse
from statistics import mean
import seaborn as sns
import matplotlib.pyplot as plt
from enum import Enum
from tax_tree import KTree


OUTPUT_FOLDER = "out"
OUTPUT_FILE = "output.txt"
REPORT_FILE = "report.txt"

parser=optparse.OptionParser(description="""Program to check a Kraken2 database.

It compares the predictions made by Kraken2 with the sequences in the database to determine if they are predicted correctly.

If a local Blast database is provided, it will compare the identity percentage of each sequence with the closest sequence. This will help you to determine wether the assignation errors are due to sequences of different species being too similar to discriminate.

The program should be run with the same Fasta files as the ones used to build the Kraken2 and Blast databases. The sequences should have the accession ID in the header.""")

parser.add_option('-k', '--kraken-db', help='The Kraken database to check.', type="string")
parser.add_option('-b', '--blast-db', help='(Optional) A local Blast database used to find the closest neighbor of the sequences. It must contain the sequences with the accession ID information (Option -parse_seqids when building the database). If not provided, the comparison will not be performed.', type="string")
parser.add_option('-f', '--fasta-files', help='The Fasta file or directory containing the sequences to check. Headers must contain the same accession ID as in the database.', type="string")
parser.add_option('-n', '--n-tests', help='The number of sequences to use for the test. If not provided, all the sequences will be used.', type="int")

parser.add_option('-c', '--confidence', help='(Optional) Confidence Kraken parmeter', type="float")
parser.add_option('-g', '--minimum-hit-groups', help='(Optional) Minimum hit groups Kraken parmeter', type="int")
parser.add_option('-q', '--minimum-base-quality', help='(Optional) Minimum base quality Kraken parmeter', type="int")

(opts,args) = parser.parse_args()

def usage():
    parser.print_help()

def get_closest(seq_id):
    if blast_db_path is None:
        return -1
    tax_id = tax_ids.get(seq_id, 0)
    blast_search = subprocess.getoutput(f"blastdbcmd -db {blast_db_path} -entry {seq_id} | blastn -db {blast_db_path} -query -  -outfmt \"6 sseqid pident\"").split("\n")
    search_results = sorted([element.split("\t") for element in blast_search], key=lambda x: x[1], reverse=True)
    
    for result in search_results:
        accession_id = result[0].split("|")[1]
        identity = float(result[1])
        if tax_ids.get(accession_id, 0)!=tax_id:
            return identity
    return 0.0

kraken_db_path = opts.kraken_db
blast_db_path = opts.blast_db
test_path = opts.fasta_files
n_tests = opts.n_tests
if kraken_db_path is None or test_path is None:
    usage()
    sys.exit(1)

if os.path.isdir(test_path):
    fasta_files = [os.path.join(test_path, file_name) for file_name in os.listdir(test_path)]
elif os.path.isfile(test_path):
    fasta_files = [test_path]
else:
    print("File not found:", test_path)
    sys.exit(1)

tax_ids = {}
with open(os.path.join(kraken_db_path, 'seqid2taxid.map'), 'r') as tax_ids_file:
    lines = tax_ids_file.readlines()
    for line in lines:
        sline = line.strip().split('\t')
        tax_ids[sline[0]] = sline[1]


sequences = []
for fasta_file in fasta_files:
    sequences.extend(SeqIO.parse(open(fasta_file, mode='r'), 'fasta'))

# Check sequences
for i in reversed(range(len(sequences))):
    sequence = sequences[i]
    if sequence.id not in tax_ids:
        del sequences[i]
    

# Select n_tests random sequences
if n_tests is None:
    test_seqs = sequences
else:
    test_seqs = [random.choice(sequences) for _ in range(n_tests)]

with open("check-db.fasta", "w") as test_file:
    SeqIO.write(test_seqs, test_file, "fasta")

# Kraken
command = f"kraken2 --db {kraken_db_path}{'' if opts.minimum_hit_groups is None else (' --minimum-hit-groups ' + str(opts.minimum_hit_groups))}{'' if opts.confidence is None else (' --confidence ' + str(opts.confidence))}{'' if opts.minimum_base_quality is None else (' --minimum-base-quality ' + str(opts.minimum_base_quality))} --report check-db.k2report check-db.fasta >check-db.kraken2"
print(command)
os.system(command)

# Read file
with open('check-db.kraken2', 'r') as kraken_file:
    lines = kraken_file.readlines()
    total = 0
    ktree = KTree(kraken_db_path)
    class Pred(Enum):
        GOOD = "good"
        TOO_LOW = "too low"
        TOO_HIGH = "too high"
        BAD_BRANCH = "bad branch"
    counts = {Pred.GOOD:0, Pred.TOO_LOW:0, Pred.TOO_HIGH:0, Pred.BAD_BRANCH: 0}
    identities = {Pred.GOOD:[], Pred.TOO_LOW:[], Pred.TOO_HIGH:[], Pred.BAD_BRANCH: []}

    for line in tqdm(lines):
        sline = line.strip().split('\t')
        access_id = sline[1]
        predicted_tax_id = sline[2]
        expected_tax_id = tax_ids[access_id]
        total += 1
        closest = get_closest(access_id)

        if predicted_tax_id==expected_tax_id:
            prediction = Pred.GOOD
        elif ktree.is_parent(expected_tax_id, predicted_tax_id):
            prediction = Pred.TOO_LOW
        elif ktree.is_parent(predicted_tax_id, expected_tax_id):
            prediction = Pred.TOO_HIGH
        else:
            prediction = Pred.BAD_BRANCH
            print(f"Bad prediction: {expected_tax_id} {predicted_tax_id}")

        counts[prediction] = counts[prediction]+1
        if closest==0.0:
            print(f"No closest neighbor found for {access_id} ({prediction})")
        elif closest!=-1:
            identities[prediction].append(closest)
print(f"Good:       {counts[Pred.GOOD]}/{total} ({100*counts[Pred.GOOD]/total}%)")
print(f"Too low:    {counts[Pred.TOO_LOW]}/{total} ({100*counts[Pred.TOO_LOW]/total}%)")
print(f"Too high:   {counts[Pred.TOO_HIGH]}/{total} ({100*counts[Pred.TOO_HIGH]/total}%)")
print(f"Bad branch: {counts[Pred.BAD_BRANCH]}/{total} ({100*counts[Pred.BAD_BRANCH]/total}%)")

os.makedirs(OUTPUT_FOLDER, exist_ok=True)
with open(f"{OUTPUT_FOLDER}/{REPORT_FILE}", 'a') as file:
    file.write(f"""{command}
Good:       {counts[Pred.GOOD]}/{total} ({100*counts[Pred.GOOD]/total}%)
Too low:    {counts[Pred.TOO_LOW]}/{total} ({100*counts[Pred.TOO_LOW]/total}%)
Too high:   {counts[Pred.TOO_HIGH]}/{total} ({100*counts[Pred.TOO_HIGH]/total}%)
Bad branch: {counts[Pred.BAD_BRANCH]}/{total} ({100*counts[Pred.BAD_BRANCH]/total}%)

""")
with open(f"{OUTPUT_FOLDER}/{OUTPUT_FILE}", 'a') as file:
    file.write(f"""db: {kraken_db_path}
minimum-hit-groups: {opts.minimum_hit_groups}
confidence: {opts.confidence}
minimum-base-quality: {opts.minimum_base_quality}
counts: {counts}
identities: {identities}

""")



if not blast_db_path is None:
    sns.boxplot(
        x=['Good'] * len(identities[Pred.GOOD]) + ['Too low'] * len(identities[Pred.TOO_LOW]) + ['Too high'] * len(identities[Pred.TOO_HIGH]) + ['Bad branch'] * len(identities[Pred.BAD_BRANCH]),
        y=identities[Pred.GOOD] + identities[Pred.TOO_LOW] + identities[Pred.TOO_HIGH] + identities[Pred.BAD_BRANCH])
    plt.xlabel('Prediction')
    plt.ylabel('Identity (%)')
    plt.savefig(f"{OUTPUT_FOLDER}/{opts.minimum_hit_groups}-{opts.confidence}-{opts.minimum_base_quality}.png")
